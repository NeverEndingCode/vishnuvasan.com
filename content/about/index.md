---
title: "About"
date: 2020-05-05T01:14:25+05:30
draft: true
tags: ["Profession"]
showInMenu: true
---
Hey Buddy,
Thank you very much for Visiting the Page.Greatly Appreciate your Time!


Welcome to this **Zillionth Zilch** in this Never Ending Useless Web.

## Personally

**Trying to be a better Human @ Heart** out of all the other infinite things I strive to be.

Striving **Harder** everyday being a better 

- Responsible Son
- Loving Husband
- Useful Engineer
- Budding Developer
- Wandering Traveller
- Gargantuous Listener
- Voracious Reader
- Avid Thinker
- & Many More Inexplicable/Unexpected Hats I am figuring out in this Continuum!

## Professionally

Like Every Other Indian, After becoming an Engineering Graduate I landed in a job which many a times I Hated the most than Loved

- 10+ Yrs of Experience with Embedded Systems, Automotive Software and Microsoft Office Suite **:wink:** 
- Programmed in a Bunch of Languages from C,VB,C#,Perl,Python,Ruby,Winwrap,Js,Scala,Elm,Clojure with **Clojure** topping the Favourites
- Landed in Mathematical Model Design starting with LogiCAD,ASCET,MATLAB,SciLab,xCos,R,Octave
- Worked in Multiple OS Flavors from Windows,Fedora,Debian,Manjaro,Ubuntu,Kali,Tails,Zorin,Elementary but always Love **Fedora**
- Love Open Source and Used GNOME,KDE,XFCE,LXDE,Sugar but an ardent Fan of **GNOME**
- Created a Handful of Tools
- Had the Opportunity to work with Quite a Few Intelligent and Extraordinary Minds which I always Cherish
- Never Loved Leadership Role better than the Last 3 Years

> **[Resume](static/Vishnu_Vasan_Nehru_EN_2020_05_05.pdf)**

Currently in a **Mid-Life Crisis** planning the best possible transition both Professionally and Personally.

## Favourite Quote - One among the Many

> ###### அச்சம் தவிர்! 
> ###### நையப்புடை! 
> ###### மானம் போற்று! 
> ### ரௌத்திரம் பழகு! 
> ###### நேர்பட பேசு! 


